<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Listings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-listings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_id',
            'classified_cat_id',
            'sub_category',
            'user_id',
            'image_count',
            'ad_type',
            'title',
            'description:ntext',
            'search_options:ntext',
            'currency',
            'price',
            'search_price',
            'price_suffix',
            'duration',
            'town_id',
            'county_id',
            'item_status',
            'telephone',
            'email:email',
            'mobile',
            'other',
            'status',
            'order',
            'bump',
            'bump_count',
            'featured',
            'date_sold',
            'views',
            'start_date',
            'end_date',
            'make_id',
            'auto_make',
            'model_id',
            'auto_model',
            'year',
            'fuel',
            'body_type',
            'colour',
            'engine_size',
            'version',
            'date_added',
            'business_plus_url:url',
            'edition_one',
            'edition_two',
            'edition_three',
            'edition_four',
            'edition_date',
            'buy_now_url:url',
            'transmission',
            'co2_emissions',
            'nct_date',
            'tax_date',
            'mileage',
        ],
    ]) ?>

</div>
