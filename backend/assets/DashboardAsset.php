<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
        'Ionicons/css/ionicons.min.css',
        'css/AdminLTE.min.css',
        'css/skins/skin-blue.min.css',
        'css/main.min.css'
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/adminlte.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
