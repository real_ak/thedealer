<?php
return [
    'adminEmail' => 'tesdata167@gmail.com',
    'supportEmail' => 'testdata167@gmail.com',
    'users.activationTokenExpire' => 3600,
    //    currency symbols
    'GBP'    => '&pound;',
    'EUR'    => '&euro;',
    'P.O.A'  => 'P.O.A',
    'FREE'   => 'FREE',
];
