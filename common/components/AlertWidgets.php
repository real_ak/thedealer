<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/18/2018
 * Time: 4:26 PM
 */
namespace common\components;

use yii\base\Widget;
use Yii;

class AlertWidgets extends Widget
{
    public $alertTypes= [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        return $this->render('alert', [
            'alertTypes' => $this->alertTypes,
            'flashes'   => $flashes
        ]);
    }

}