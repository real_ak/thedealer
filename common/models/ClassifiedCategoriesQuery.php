<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ClassifiedCategories]].
 *
 * @see ClassifiedCategories
 */
class ClassifiedCategoriesQuery extends \yii\db\ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return ClassifiedCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClassifiedCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    public function getCategories()
    {
        return $this->andWhere(['parent_id' => 0, 'temp_parent_id' => 0, 'temp_parent_id_2' => 0])
                    ->orderBy(['order' => SORT_ASC]);
    }

    public function childCategories($id)
    {
        return $this->andWhere('parent_id = :parent_id',[':parent_id' => $id])
            ->orderBy(['order'=>SORT_ASC]);
    }
    
    public function subChildCategories($id)
    {
        return $this->andWhere('temp_parent_id = :temp_parent_id',[':temp_parent_id' => $id])
            ->orderBy(['order'=>SORT_ASC]);
    }

}
