<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $user_type
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $token
 * @property string $token_ip_address
 * @property string $hint
 * @property string $contact_name
 * @property string $company_name
 * @property int $image_count
 * @property string $address_one
 * @property string $address_two
 * @property int $town_id
 * @property string $town
 * @property int $county_id
 * @property string $county
 * @property string $postcode
 * @property string $telephone
 * @property string $mobile
 * @property string $fax
 * @property string $website
 * @property int $marketing_id
 * @property string $other
 * @property string $account_balance
 * @property string $newsletter
 * @property string $user_lat
 * @property string $user_long
 * @property int $directory_cat_id
 * @property string $category
 * @property string $description
 * @property string $status
 * @property string $directory_status
 * @property string $directory_link
 * @property int $directory_end_date
 * @property int $date_added
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property int $seo_id
 * @property string $business_plus
 * @property string $allow_buy_now
 */
class Users extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE = 'active';
    const YES = 'yes';
    const NO = 'no';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'newsletter', 'description', 'status', 'directory_status', 'directory_link', 'business_plus', 'allow_buy_now'], 'string'],
//            [['username', 'password', 'token', 'token_ip_address', 'hint', 'contact_name', 'image_count', 'town', 'county', 'postcode', 'mobile', 'fax', 'website', 'marketing_id', 'other', 'user_lat', 'user_long', 'directory_cat_id', 'category', 'description', 'directory_end_date', 'meta_title', 'meta_keywords', 'meta_description', 'seo_id'], 'required'],
            [['image_count', 'town_id', 'county_id', 'marketing_id', 'directory_cat_id', 'directory_end_date', 'date_added', 'seo_id'], 'integer'],
            [['account_balance'], 'number'],
            [['email', 'username', 'hint', 'address_one', 'address_two', 'website'], 'string', 'max' => 100],
            [['password', 'token'], 'string', 'max' => 255],
            [['token_ip_address'], 'string', 'max' => 16],
            [['contact_name', 'town', 'county', 'other', 'category'], 'string', 'max' => 60],
            [['company_name'], 'string', 'max' => 50],
            [['postcode'], 'string', 'max' => 8],
            [['telephone', 'mobile', 'fax', 'user_lat', 'user_long'], 'string', 'max' => 20],
            [['meta_title', 'meta_keywords'], 'string', 'max' => 200],
            [['meta_description'], 'string', 'max' => 150],
            [['user_type', 'email', 'username'], 'unique', 'targetAttribute' => ['user_type', 'email', 'username']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => 'User Type',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'token' => 'Token',
            'token_ip_address' => 'Token Ip Address',
            'hint' => 'Hint',
            'contact_name' => 'Contact Name',
            'company_name' => 'Company Name',
            'image_count' => 'Image Count',
            'address_one' => 'Address One',
            'address_two' => 'Address Two',
            'town_id' => 'Town ID',
            'town' => 'Town',
            'county_id' => 'County ID',
            'county' => 'County',
            'postcode' => 'Postcode',
            'telephone' => 'Telephone',
            'mobile' => 'Mobile',
            'fax' => 'Fax',
            'website' => 'Website',
            'marketing_id' => 'Marketing ID',
            'other' => 'Other',
            'account_balance' => 'Account Balance',
            'newsletter' => 'Newsletter',
            'user_lat' => 'User Lat',
            'user_long' => 'User Long',
            'directory_cat_id' => 'Directory Cat ID',
            'category' => 'Category',
            'description' => 'Description',
            'status' => 'Status',
            'directory_status' => 'Directory Status',
            'directory_link' => 'Directory Link',
            'directory_end_date' => 'Directory End Date',
            'date_added' => 'Date Added',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'seo_id' => 'Seo ID',
            'business_plus' => 'Business Plus',
            'allow_buy_now' => 'Allow Buy Now',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by activation token
     *
     * @param string $token activation token
     * @return static|null
     */
    public static function findByActivationToken($token)
    {
        if (!static::isActivationTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'token' => $token,
            'status' => self::STATUS_INACTIVE,
        ]);
    }

    /**
     * Finds out if activation token is valid
     *
     * @param string $token activation token
     * @return bool
     */
    public static function isActivationTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['users.activationTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
     /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        $oldPass = sha1($password);
        if($oldPass == $this->password){
            return true;
        } else {
            return Yii::$app->security->validatePassword($password, $this->password);
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Set status to the model
     *
     * @param string $status
     */
    public function setStatus($status='inactive')
    {
        $this->status = $status;
    }

    /**
     * Generates new activation token
     */
    public function generateActivationToken()
    {
        $this->token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    /**
     * Generates new password
     */
    public function generateNewPassword()
    {
        $this->password = Yii::$app->security->generatePasswordHash(
                            Yii::$app->security->generateRandomString());
    }

    /**
     * Removes activation token
     */
    public function removeActivationToken()
    {
        $this->token = null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings()
    {
        return $this->hasMany(ClassifiedListings::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounty()
    {
        return $this->hasOne(LocationsCounties::className(), ['id' => 'county_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(LocationsTowns::className(), ['id' => 'town_id']);
    }

}
