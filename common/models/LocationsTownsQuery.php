<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/12/2018
 * Time: 1:22 PM
 */

namespace common\models;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[LocationsTowns]].
 *
 * @see LocationsTowns
 */

class LocationsTownsQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LocationsTowns[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function getTown($id)
    {
        return $this->andWhere(['id'=>$id]);
    }

}