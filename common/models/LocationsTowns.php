<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locations_towns".
 *
 * @property int $id
 * @property int $county_id
 * @property string $town
 * @property string $status
 * @property int $date_added
 * @property double $latitude
 * @property double $longitude
 * @property string $slug
 *
 * @property ClassifiedListings[] $classifiedListings
 * @property LocationsCounties $county
 * @property Users[] $users
 */
class LocationsTowns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_towns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county_id', 'town', 'date_added'], 'required'],
            [['county_id', 'date_added'], 'integer'],
            [['status'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['town', 'slug'], 'string', 'max' => 40],
            [['county_id', 'town'], 'unique', 'targetAttribute' => ['county_id', 'town']],
            [['county_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocationsCounties::className(), 'targetAttribute' => ['county_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'county_id' => 'County ID',
            'town' => 'Town',
            'status' => 'Status',
            'date_added' => 'Date Added',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'slug' => 'Slug',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LocationsTownsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LocationsTownsQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifiedListings()
    {
        return $this->hasMany(ClassifiedListings::className(), ['town_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounty()
    {
        return $this->hasOne(LocationsCounties::className(), ['id' => 'county_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['town_id' => 'id']);
    }
}
