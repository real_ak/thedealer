<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/11/2018
 * Time: 4:56 PM
 */

namespace frontend\components;


use yii\base\Widget;
use common\models\ClassifiedListings;

/**
 * Class ItemWidget
 * @property ClassifiedListings $model
 * @property string $cssClass
 * @package frontend\components
 */

class ItemWidget extends Widget
{
    public $model;
    public $cssClass;

    function run()
    {
        return $this->render('item', [
            'model' => $this->model,
            'cssClass' => $this->cssClass,
        ]);
    }

}