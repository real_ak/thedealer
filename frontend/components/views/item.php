<?php
/**
 * Created by PhpStorm.
 * User: i5 Developer
 * Date: 9/11/2018
 * Time: 5:10 PM
 */

use frontend\components\ItemWidget;
use common\models\ClassifiedListings;
use common\models\Images;
use common\models\LocationsTowns;
use common\models\LocationsCounties;
use yii\helpers\Html;
use yii\helpers\Url;



$itemUrl = Url::to(['view', 'id' => $model->id]);

?>

<div class="<?= $cssClass; ?>">
    <a class="ads-grid-content border" href="<?= $itemUrl; ?>">
        <?php
        $images = Images::find()->select('image_name')->getImages(ClassifiedListings::tableName(),$model->id)->one();
        $imagePath = \Yii::getAlias('@webroot').'/uploads/small/'.$images->image_name;
        ?>
        <?php if (file_exists($imagePath)) { ?>
            <?= Html::img('/uploads/small/'.$images->image_name, ['alt'=> $model->title, 'title'=>$model->title, 'class'=>'ads_img' ]); ?>
        <?php } else { ?>
            <?= Html::img('/images/no_images_thumb.jpg', ['alt'=> $model->title, 'title'=>$model->title, 'class'=>'ads_img' ]) ?>
        <?php } ?>
        <?php
            $price = Yii::$app->params[$model->currency].$model->price;
        ?>
        <div class="meta-info border-bottom">
            <span class="price"><?= $price; ?></span>
            <span class="watch float-right">
                <i class="fal fa-heart fa-fw fa-lg"></i>
            </span>
        </div>
        <h4 class="ad-title"><?= $model->title; ?></h4>
        <?php
            $town = LocationsTowns::find()->select('town')->getTown($model->town_id)->one();
            $county = LocationsCounties::find()->select('county')->getCounty($model->county_id)->one();
            $location = $town->town.', '.$county->county;
        ?>
        <p class="location"><?= $location; ?></p>
    </a>
</div>
