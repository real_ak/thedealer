<?php

use frontend\components\LogoWidget;
use yii\helpers\Url;

?>

<a class="navbar-brand" href="<?= Url::home(); ?>">
    <img class="img-fluid" src="<?= Url::to('@web/images/thedealer.png'); ?>" alt="">
</a>
<button class="navbar-toggler d-block d-md-none" data-toggle="collapse" data-target="#mobilenavbar">
	<i class="fas fa-bars fa-lg"></i>
</button>