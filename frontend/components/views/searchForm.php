<?php

use frontend\components\SearchFormWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['id'=>'search','options' =>['class'=>'form-inline my-0']]); ?>
<input class="form-control" type="search" placeholder="Enter your search term..." aria-label="Search">
<?= Html::submitButton('Search', ['class' => 'btn my-2 my-sm-0', 'name' => 'search-button']) ?>
<?php ActiveForm::end(); ?>
