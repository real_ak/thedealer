<?php

namespace frontend\components;

use yii\base\Widget;

class SearchFormWidget extends Widget
{
    public function run()
    {
        return $this->render('searchForm');
    }
}