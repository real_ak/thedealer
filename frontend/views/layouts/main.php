<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\components\AlertWidgets;
use common\models\ClassifiedCategories;
use frontend\components\LogoWidget;
use frontend\components\HeaderMenusWidget;
use frontend\components\SearchFormWidget;
use frontend\components\FeaturedMenuWidget;
use frontend\components\MobileSearchWidget;
use frontend\components\CategoryListWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        
        <div class="wrap">
            <header>
                <div class="top-bar">
                    <div class="container">
                        <div class="top-bar-content navbar navbar-default justify-content-between">
                             <?= LogoWidget::widget(); ?>
                            <div class="topbar-left d-md-flex flex-row d-none">
                                <?= SearchFormWidget::widget(); ?>
                                <ul>
                                    <li class="account d-none d-md-inline-block">
                                        <?php if (Yii::$app->user->isGuest){ ?>
                                            <a href="<?= Url::to('signup'); ?>">Sign up</a> | <a href="<?= Url::to('login')?>">Login</a>

                                        <?php } else { ?>
                                            <?= html::a('My Account', '#'); ?> | <a href="<?= Url::to('site/logout')?>" data-method="post">Logout</a>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <?= HeaderMenusWidget::widget(); ?>
            <div class="container">
                <div class="d-block d-lg-none">
                    <?= FeaturedMenuWidget::widget(); ?>
                </div>
                <?= MobileSearchWidget::widget(); ?>
            </div>
            <?php if(Url::current() == '/classified/index' || Url::current() == Url::home()){ ?>
                <div class="categories-filter">
                    <div class="container pt-5 pb-5">
                        <div class="row">
                            <div class="col-md-10">
                                <?= CategoryListWidget::widget(); ?>
                            </div>
                            <div class="col-md-2">
                                <a href="<?= Url::to('create') ?>" class="btn btn-default btn-block btn-lg">Place an ad <i class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="container">
                <?= AlertWidgets::widget(); ?>
                <div class="d-lg-block d-none">
                    <?= FeaturedMenuWidget::widget(); ?>
                </div>
            </div>
            <div class="content">
                <?= $content ?>
            </div>
        </div>
            
        <footer class="footer">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-4 col-sm-6 site-map mb-4">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-6">
                                <h4>TheDealer.ie</h4>
                                <div class="empty-space"></div>
                                <ul class="nav flex-column">
                                    <li><?= html::a('Place an Ad', '#'); ?></li>
                                    <li><?= html::a('Find Business', '#'); ?></li>
                                    <li><?= html::a('Latest Edition', '#'); ?></li>
                                    <li><?= html::a('Terms &amp; condtions', '#'); ?></li>
                                    <li><?= html::a('FAQ', '#'); ?></li>
                                    <li><?= html::a('About', Url::to('about')); ?></li>
                                    <li><?= html::a('Get in Touch', Url::to('contact')); ?></li>
                                </ul>
                                <div class="empty-space"></div>
                                <div class="empty-space"></div>
                                <h4>My Account</h4>
                                <div class="empty-space"></div>
                                <ul class="nav flex-column">
                                    <?php if (Yii::$app->user->isGuest){ ?>
                                        <li><?= html::a('Login/Register', Url::to('login')); ?></li>
                                    <?php } ?>
                                    <li><?= html::a('Dashboard', '#'); ?></li>
                                    <li><?= html::a('My Ads', '#'); ?></li>
                                    <li><?= html::a('My Whatchlist', '#'); ?></li>
                                    <li><?= html::a('My Online Issues', '#'); ?></li>
                                    <li><?= html::a('Buy Credit', '#'); ?></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 border-left">
                                <h4>Search Ads</h4>
                                <div class="empty-space"></div>
                                <?php $categories = ClassifiedCategories::find()->getCategories()->all(); ?>
                                <ul class="nav flex-column">
                                    <?php foreach ($categories as $category) : ?>
                                        <li><?= html::a($category->category, '#'); ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 contact-us mb-4">
                        <h4>Sign up to get the latest news</h4>
                        <div class="empty-space"></div>
                        <div class="subscribe">
                            <form name="newslatter-signup" action="#">
                                <div class="input-group mb-3">
                                    <input type="email" class="form-control input-lg" placeholder="Email Address"/>
                                    <div class="input-group-append">
                                        <button class="btn btn-default btn-lg" type="submit">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                            <a class="btn btn-lg btn-block feedback" href="#">Give us your feedback</a>
                        </div>
                        <div class="empty-space"></div>
                        <div class="empty-space"></div>
                        <div class="empty-space"></div>
                        <div class="contact">
                            <h4>Need Help?</h4>
                            <p>We're here to<br>answer any questions you have</p>
                            <div class="info mail-us">
                                <i class="fal fa-envelope fa-fw fa-lg"></i>
                                <a href="mailto:info@thedealer.ie">
                                    info@thedealer.ie
                                </a>
                            </div>
                            <div class="info call-us">
                                <span class="fa-layers fa-fw fa-lg">
                                    <i class="far fa-circle"></i>
                                    <i class="fa-inverse fas fa-phone" data-fa-transform="shrink-8  flip-h"></i>
                                </span>
                                <a href="tel:+353 (0)74 91 26410">
                                     +353 (0)74 91 26410
                                </a>
                            </div>
                            <div class="follow-us mt-3">
                                <a href="https://www.facebook.com/TheDealerFreeAdsPaper">
                                    <i class="fab fa-facebook-f fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                </a>
                                <a href="https://twitter.com/thedealer_ie">
                                    <i class="fab fa-twitter fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                </a>
                                <a href="https://www.facebook.com/TheDealerFreeAdsPaper">
                                    <i class="fab fa-google-plus-g fa-4x" data-fa-transform="shrink-6" data-fa-mask="fas fa-circle"></i>
                                </a>
                            </div>
                        </div>
                        
                        
                    </div>

                    <div class="col-md-4 col-sm-12 mb-4">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 mb-3">
                                <div class="contact-office">
                                    <h4>Our Offices</h4>
                                    <div class="media mb-4">
                                        <?= Html::img('/images/map.jpg', ['alt'=>'Address', 'class'=>'map mr-2']); ?>
                                        <div class="media-body">
                                            <p>Letterkenny, Donegal <br> +353 (0)74 9126410 / 081 830 3100</p>
                                            <?= Html::a('Get Direction', '#', ['class'=>'btn direction-link']) ?>
                                        </div>
                                    </div>
                                    <div class="sperator"></div>
                                    <div class="media mt-4">
                                        <?= Html::img('/images/map.jpg', ['alt'=>'Address', 'class'=>'map mr-2']); ?>
                                        <div class="media-body">
                                            <p>Letterkenny, Donegal <br> +353 (0)74 9126410 / 081 830 3100</p>
                                            <?= Html::a('Get Direction', '#', ['class'=>' btn direction-link']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <div class="fb-page" data-href="https://www.facebook.com/TheDealerFreeAdsPaper" data-tabs="timeline" data-height="250" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                                    <blockquote cite="https://www.facebook.com/TheDealerFreeAdsPaper" class="fb-xfbml-parse-ignore">
                                        <a href="https://www.facebook.com/TheDealerFreeAdsPaper">The Dealer</a>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copy-rights">
            <div class="container">
                <p class="text-center">&copy; <?= Html::a(Html::encode(Yii::$app->name), Yii::$app->homeUrl) ?> <?= date('Y') ?></p>
            </div>
        </div>
            
        <?php $this->endBody() ?>
        <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
<?php $this->endPage() ?>
