<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ClassifiedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Classified Listings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="recent-ads">
        <div class="container">
            <div class="row">
                <div class="col-md-9 ads-listing">
                    <h3 class="section-heading">Recent Ads</h3>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView'     => '_item',
                        'layout'       => '<div class="row no-gutters">{items}</div>',
                        'itemOptions'  => [
                                'class' => 'col-md-4 col-sm-4 col-6 ads-grid',
                        ],
                    ]); ?>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
    
</div>
