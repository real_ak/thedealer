<?php

/** @var Item $model */

use common\models\Item;
use frontend\components\ItemWidget;

echo ItemWidget::widget(['model' => $model]);