<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classified-listings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'classified_cat_id')->textInput() ?>

    <?= $form->field($model, 'sub_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'image_count')->textInput() ?>

    <?= $form->field($model, 'ad_type')->dropDownList([ 'private' => 'Private', 'business' => 'Business', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'search_options')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_suffix')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'town_id')->textInput() ?>

    <?= $form->field($model, 'county_id')->textInput() ?>

    <?= $form->field($model, 'item_status')->dropDownList([ 'for sale' => 'For sale', 'wanted' => 'Wanted', 'sold' => 'Sold', 'to let' => 'To let', 'available' => 'Available', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'other')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', 'sold' => 'Sold', 'cancelled' => 'Cancelled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'bump')->textInput() ?>

    <?= $form->field($model, 'bump_count')->textInput() ?>

    <?= $form->field($model, 'featured')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'date_sold')->textInput() ?>

    <?= $form->field($model, 'views')->textInput() ?>

    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?= $form->field($model, 'make_id')->textInput() ?>

    <?= $form->field($model, 'auto_make')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model_id')->textInput() ?>

    <?= $form->field($model, 'auto_model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'fuel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'colour')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'engine_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_added')->textInput() ?>

    <?= $form->field($model, 'business_plus_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edition_one')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'edition_two')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'edition_three')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'edition_four')->dropDownList([ 'no' => 'No', 'yes' => 'Yes', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'edition_date')->textInput() ?>

    <?= $form->field($model, 'buy_now_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transmission')->dropDownList([ 'M' => 'M', 'A' => 'A', 'G' => 'G', 'C' => 'C', 'S' => 'S', 'T' => 'T', 'Other' => 'Other', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'co2_emissions')->textInput() ?>

    <?= $form->field($model, 'nct_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tax_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mileage')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
