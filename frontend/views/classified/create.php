<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClassifiedListings */

$this->title = Yii::t('app', 'Create Classified Listings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Listings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-listings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
