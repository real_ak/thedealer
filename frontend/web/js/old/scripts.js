$(document).ready(function(){
    $('#signupform-company_name').hide();
    $('input[name="SignupForm[user_type]"][value="private"]').prop("checked",true);
    $('input[name="SignupForm[user_type]"][value="private"]').parent('.radio-inline').addClass('check');
    $('input[name="SignupForm[user_type]"]').on('click', function(){
        $('.radio-inline').removeClass("check");
        var value = $('input[name="SignupForm[user_type]"]:checked').val();
        if (value === "business"){
            $('#signupform-company_name').show();
            $('#signupform-company_name').addClass('required')
            $(this).parent('.radio-inline').addClass("check");
        } else if (value === "private"){
            $('#signupform-company_name').hide();
            $(this).parent('.radio-inline').removeClass("check");
            $(this).parent('.radio-inline').addClass("check");
        }
    });
    $('label input[type="checkbox"]').change( function(){
        if($(this).is(":checked")){
            $(this).parent('label').addClass('checked');
        }
        else if($(this).is(":not(:checked)")){
            $(this).parent('label').removeClass('checked');
        }
    });
    $('#signupform-show_pass').change( function(){
        if($(this).is(":checked")){
            $('#signupform-password').prop('type', 'text');
        }
        else if($(this).is(":not(:checked)")){
            $('#signupform-password').prop('type', 'password');
        }
    });
});
