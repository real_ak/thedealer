$(document).ready(function(){
    $('#signupform-company_name').hide();
    
    $('input[type="radio"]:checked').parent().addClass('check');


    $('input[type="radio"]').on('click', function(){
        $('input[type="radio"]').parent().removeClass("check");
        $(this).parent().addClass("check");
    });


    $('input[name="SignupForm[user_type]"]').on('click', function(){
        $('.radio-inline').removeClass("check");
        var companyName = $('#signupform-company_name');
        var val = $('input[name="SignupForm[user_type]"]:checked').val();
        if (val === "business"){
            $(companyName).show();
            $(companyName).addClass('required');
            $(this).parent('.radio-inline').addClass("check");
        } else if (val === "private"){
            $(companyName).hide();
            $(this).parent('.radio-inline').removeClass("check");
            $(this).parent('.radio-inline').addClass("check");
        }
    });
    $('label input[type="checkbox"]').change( function(){
        if($(this).is(":checked")){
            $(this).parent('label').addClass('checked');
        }
        else if($(this).is(":not(:checked)")){
            $(this).parent('label').removeClass('checked');
        }
    });
    $('#signupform-show_pass').change( function(){
        if($(this).is(":checked")){
            $('#signupform-password').prop('type', 'text');
        }
        else if($(this).is(":not(:checked)")){
            $('#signupform-password').prop('type', 'password');
        }
    });
     $('.options').select2({
         minimumResultsForSearch: -1
     });
     $('form select').select2();
});


$(function () {
    var maxHeight = 0;

    $(".contact-wrapper .contact-details").each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $(".contact-wrapper .contact-details").height(maxHeight);

    var height = $('#directory-page header').height() + $('#directory-page .search-form').height() + 32;
    $('.directory-wrapper .directories-location').css('height','calc(100vh - '+height+'px)');
});


$(document).ready(function(){
    
    $('.categories-grid > a').click(function(){
        $('.categories-grid').css('margin-bottom', '20px');
        var that = $('.categories-grid > a.active');
        $('.categories-grid > a').removeClass('active');
        var margin = $($(this).attr('href')).outerHeight(true) + 25;
        var top = $(this).outerHeight(true);
        var count = $(this).parent('.categories-grid').data('count');
        if($(this).hasClass('collapsed')){
            $(this).addClass('active');
            $(this).parent('.categories-grid').css('margin-bottom', margin+'px');
            if (count > 9) {
                if (that.parent().data('count') < 9){
                    console.log('true');
                    top = top + that.outerHeight(true) + 36;
                    console.log(top);
                } else {
                    console.log('false');
                    top = top + $('.categories-grid[data-count="9"]').outerHeight(true);
                    console.log(top);
                }
            }
            $($(this).attr('href')).css('top',top+'px');
        } else {
            $(this).parent('.categories-grid').css('margin-bottom', '20px');
        }
    });

    $('.mobile-filter .filtered-head').click(function(){
        // var sidebar = $('.sidebar-filter').parent().html();
        // $('.mobile-sidebar-filter').append(sidebar);
    });

});
